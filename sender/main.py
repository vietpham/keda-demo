#!/usr/bin/env python
import pika
import sys
import os
import time
import random

rabbitmq_host = os.environ["RABBITMQ_HOST"]

with pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_host)) as connection:
    channel = connection.channel()

    channel.queue_declare(queue='task_queue', durable=True)

    while True:
        n = random.randint(1, 10)
        message = "Hello World" + "." * n
        channel.basic_publish(
            exchange='',
            routing_key='task_queue',
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=pika.DeliveryMode.Persistent
            ))
        print(f" [x] Sent {message}")
        t = random.randint(1, 10)
        time.sleep(t)
